var timeout = [];

var fadeLauncherIn, fadeLauncherOut,
	fadeNavsIn, fadeNavsOut, startButtonListener,
	theFrame, theClone;

$(document).ready(function(){
	var navs = $(".nav"),
		smallNavs = $(".small-nav"),
		greySkin = $(".nav-grey-skin"),
		launcher = $(".nav-launcher"), 
		home = $(".nav-home"), 
		play = $(".nav-play");
	
	greySkin.fadeOut(0);
	navs.fadeOut(0);
	
	fadeNavsOut = function(){
		greySkin.fadeOut(300);
		smallNavs.addClass("transition-0-5s");
		smallNavs.addClass("nav-center");
		
		timeout[0] = setTimeout(function(){
			smallNavs.fadeOut(0);
			home.fadeOut(200);
			greySkin.fadeOut(200);
		}, 1000);
		timeout[0] = setTimeout(function(){
			smallNavs.removeClass("transition-0-5s");
			smallNavs.removeClass("nav-center");
		}, 2000);
	}
	fadeNavsIn = function(){
		if(greySkin.css("display", "none"))
		{
			greySkin.fadeIn(500);
			smallNavs.addClass("nav-center");
			navs.fadeIn(500);
			launcher.fadeOut(0);
			timeout[0] = setTimeout(function(){
				smallNavs.addClass("transition-0-5s");
				smallNavs.removeClass("nav-center");
			}, 500);
			timeout[1] = setTimeout(function(){
				smallNavs.removeClass("transition-0-5s");
			}, 2000)
		}
	}
	
	fadeLauncherIn = function(){
		launcher.fadeIn(0);
	}
	fadeLauncherOut = function(){
		launcher.fadeOut(0);
	}
	
	var navListener = function(){
		var currElem = $(this);
		
		if(currElem.hasClass("nav-home"))
		{
			hideEverythingBut($("#frame-000"));
		}
		else if(currElem.hasClass("nav-ff"))
		{
			hideEverythingBut($("#" + theFrame.attr("data-next")));
		}
		else if(currElem.hasClass("nav-fb"))
		{
			hideEverythingBut($("#" + theFrame.attr("data-prev")));
		}
		else if(currElem.hasClass("nav-close"))
		{
			fadeNavsOut();
			launcher.fadeIn(200);
		}
		else if(currElem.hasClass("nav-repeat"))
		{
			theFrame.html(theClone.html());
			hideEverythingBut(theFrame);
		}
		else if(currElem.hasClass("nav-sound"))
		{
			console.log("sound");
			//hideEverythingBut("#frame-000");
		}
		else if(currElem.hasClass("nav-help"))
		{
			console.log("going to help");
			//hideEverythingBut("#frame-000");
		}
		else if(currElem.hasClass("nav-list"))
		{
			console.log("going to show list");
			//hideEverythingBut("#frame-000");
		}
		else if(currElem.hasClass("nav-play"))
		{
			fadeNavsOut(0);
			timeout[0] = setTimeout(function(){
				fadeLauncherIn();
			}, 1000);
			startButtonListener();
		}
		else if(currElem.hasClass("nav-launcher"))
		{
			fadeNavsIn();
		}
	}
	navs.off("click", navListener);
	navs.on("click", navListener);	
});

var textFontResizer = function(){
	var mwidth = parseInt($("#main").css("width"));
	
	$("body").css("font-size", mwidth / 20 + "px");
}

