//JQUERY SELECTORS
var derButton, factsButton, labButton, links,
	menuFrame, frames, model, modelContent;

var audio = [],
	audioPieces = [];
	
//SOUNDS
var buttonSound, fireworkSound, speechSound;

//SPRITES
var injectorSprite; 

//OTHER GLOBAL DATA
var shots, currentShot;

function AudioPiece(source, start, end)
{
	this.audio = new Audio("audio/" + source + ".mp3");
	this.start = start;
	this.end = end;
	
	var currPiece = this,
		currAudio = this.audio;
	
	currAudio.addEventListener("timeupdate", function(){
		var currTime = Math.round(currAudio.currentTime);
		
		if(currTime == currPiece.end)
			currAudio.pause();
	});
}

AudioPiece.prototype.play = function()
{
	this.audio.currentTime = this.start;
	this.audio.play();
}

AudioPiece.prototype.pause = function()
{
	this.audio.pause();
}

AudioPiece.prototype.addEventListener = function(e, callBack)
{
	var currPiece = this,
		currAudio = this.audio,
		currDuration = Math.round(currAudio.duration);
	
	if(e === "ended")
	{
		console.log("else end: " + currPiece.end + ", " + currDuration);
		currAudio.addEventListener("timeupdate", function(){
			var currTime = Math.round(currAudio.currentTime);
				
			if(currTime === currPiece.end)
			{
				currAudio.pause();
				currAudio.currentTime += 3;
				callBack();
			}
		});
	}
	if(e === "timeupdate")
	{
		currPiece.audio.addEventListener("timeupdate", callBack);
	}
}

AudioPiece.prototype.getStart = function()
{
	return this.start;
}


var plusOneSec = function(audiofile)
{
	var currTime = Math.round(audiofile.currentTime);
	audiofile.currentTime = ++currTime;
}

//TIMEOUTS
var timeout = [],
	sprite = [];
var launch = [];

launch["frame-000"] = function()
{
	theFrame = $("#frame-101"),
	theClone = theFrame.clone();
	fadeNavsOut();
}

launch["frame-101"] = function()
{		
		theFrame = $("#frame-101"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ", 
		girlMouth = $(prefix + ".girl-mouth");
		
	audioPieces[0] = new AudioPiece("s2-1", 0, 6);
		
	girlMouth.fadeOut(0);
	fadeNavsIn();
	
	audioPieces[0].addEventListener("ended", function(){
		girlMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 2000);
	});
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audioPieces[0].play();
			girlMouth.fadeIn(0);
		}, 2000);
	};
}

launch["frame-102"] = function()
{		
		theFrame = $("#frame-102"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyMouth = $(prefix + ".boy-mouth");
	
	boyMouth.fadeOut(0);
	fadeNavsIn();
	
	audioPieces[0] = new AudioPiece("s2-1", 6, 20);
	audioPieces[0].addEventListener("ended", function(){
		boyMouth.fadeOut(0);
		timeout[1] = setTimeout(function(){
			fadeNavsIn();
		}, 1000);
	});
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			boyMouth.fadeIn(0);
			audioPieces[0].play();
		}, 1000);
	};
}

launch["frame-103"] = function()
{		
		theFrame = $("#frame-103"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyMouth = $(prefix + ".boy-mouth");
		
	audioPieces[0] = new AudioPiece("s2-1", 20, 25);
		
	boyMouth.fadeOut(0);
	fadeNavsIn();
	
	audioPieces[0].addEventListener("ended", function(){
		boyMouth.fadeOut(0);
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 2000);
	});
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			boyMouth.fadeIn(0);
			audioPieces[0].play();
		}, 1000);
	};
}

launch["frame-104"] = function()
{		
		theFrame = $("#frame-104"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ", 
		boyMouth = $(prefix + ".boy-mouth"), 
		girlMouth = $(prefix + ".girl-mouth"),
		girlMouthIdle = $(prefix + ".girl-mouth-idle"),
		vinegar = $(prefix + ".vinegar");
		
	audioPieces[0] = new AudioPiece("s2-1", 24, 38);
		
	boyMouth.fadeOut(0);
	girlMouth.fadeOut(0);
	vinegar.fadeOut(0);
	fadeNavsIn();
	
	audioPieces[0].addEventListener("ended", function(){
		girlMouth.fadeOut(0);
		girlMouthIdle.fadeIn(0);
		vinegar.fadeOut(1000);
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 2000);
	});
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			girlMouthIdle.fadeOut(0);
			girlMouth.fadeIn(0);
			audioPieces[0].play();
		}, 1000);
		timeout[1] = setTimeout(function(){
			vinegar.fadeIn(500);
		}, 3000);
	};
}

launch["frame-105"] = function()
{		
		theFrame = $("#frame-105"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		girlMouth = $(prefix + ".girl-mouth"),
		girlMouthIdle = $(prefix + ".girl-mouth-idle"),
		demoPics = $(prefix + ".demo-pic"), 
		lemon = $(prefix + ".lemon"), 
		cola = $(prefix + ".cola"), 
		orange = $(prefix + ".orange");
	
	demoPics.fadeOut(0);
	girlMouth.fadeOut(0);
	fadeNavsIn();
	
	audioPieces[0] = new AudioPiece("s2-1", 38, 55);
	
	audioPieces[0].addEventListener("ended", function(){
		girlMouth.fadeOut(0);
		girlMouthIdle.fadeIn(0);
		lemon.fadeOut(500);
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 2000);
	});
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audioPieces[0].play();
			girlMouthIdle.fadeOut(0);
			girlMouth.fadeIn(0);
		}, 1000);
		timeout[1] = setTimeout(function(){
			cola.fadeIn(500);
		}, 3000);
		timeout[2] = setTimeout(function(){
			cola.fadeOut(500);
			orange.fadeIn(500);
		}, 5000);
		timeout[2] = setTimeout(function(){
			orange.fadeOut(500);
			lemon.fadeIn(500);
		}, 8000);
	};
}

launch["frame-106"] = function()
{		
		theFrame = $("#frame-106"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		girlMouth = $(prefix + ".girl-mouth"),
		girlMouthIdle = $(prefix + ".girl-mouth-idle"),
		demoPics = $(prefix + ".demo-pic"),
		ant = $(prefix + ".ant"), 
		acids = $(prefix + ".acids"),
		burntHand = $(prefix + ".burnt-hand"),
		handWash = $(prefix + ".hand-wash"),
		caustic = $(prefix + ".caustic"), 
		cross = $(prefix + ".cross");
	
	sprite[0] = new Motio(acids[0], {
		"fps": "2", 
		"frames": 5
	});
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	sprite[1] = new Motio(handWash[0], {
		"fps": "2", 
		"frames": 12
	});
	sprite[1].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	demoPics.fadeOut(0);
	girlMouth.fadeOut(0);
	fadeNavsIn();
	
	audio[0] = new Audio("audio/s3-1.mp3");
	
	audio[0].addEventListener("ended", function(){
		girlMouth.fadeOut(0);
		girlMouthIdle.fadeIn(0);
		handWash.fadeOut(500);
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 2000);
	});
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audio[0].play();
			girlMouthIdle.fadeOut(0);
			girlMouth.fadeIn(0);
		}, 1000);
		timeout[1] = setTimeout(function(){
			ant.fadeIn(500);
		}, 6000);
		timeout[2] = setTimeout(function(){
			ant.fadeOut(500);
			acids.fadeIn(500);
		}, 9000);
		timeout[3] = setTimeout(function(){
			sprite[0].play();
		}, 11000);
		timeout[4] = setTimeout(function(){
			acids.fadeOut(0);
			caustic.fadeIn(0);
		}, 18000);
		timeout[5] = setTimeout(function(){
			caustic.fadeOut(0);
			cross.fadeIn(0);
		}, 20000);
		timeout[6] = setTimeout(function(){
			cross.fadeOut(0);
			burntHand.fadeIn(500);
		}, 25000);
		timeout[7] = setTimeout(function(){
			burntHand.fadeOut(500);
			handWash.fadeIn(500);
		}, 29000);
		timeout[7] = setTimeout(function(){
			sprite[1].play();
		}, 30000);
	};
}

launch["frame-107"] = function()
{		
		theFrame = $("#frame-107"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		girlMouth = $(prefix + ".girl-mouth"),
		girlMouthIdle = $(prefix + ".girl-mouth-idle"),
		demoPics = $(prefix + ".demo-pic"), 
		acidPlusMetal = $(prefix + ".acid-plus-metal"), 
		acidPlusMagnesium = $(prefix + ".acid-plus-magnesium"),
		videoContainer = $(prefix + ".video-container"),
		video = $(prefix + ".video");
	
	video.attr("width", videoContainer.css("width"));
	video.attr("height", videoContainer.css("height"));
	
	$(window).resize(function(){
		video.attr("width", videoContainer.css("width"));
		video.attr("height", videoContainer.css("height"));
	});
	
	sprite[0] = new Motio(acidPlusMetal[0], {
		"fps": "2", 
		"frames": 8
	});
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	sprite[1] = new Motio(acidPlusMagnesium[0], {
		"fps": "2", 
		"frames": 10
	});
	sprite[1].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	demoPics.fadeOut(0);
	girlMouth.fadeOut(0);
	fadeNavsIn();
	videoContainer.fadeOut(0);
	
	audioPieces[0] = new AudioPiece("s4-1", 0, 29);
	
	video[0].addEventListener("ended", function(){
		girlMouth.fadeOut(0);
		girlMouthIdle.fadeIn(0);
		videoContainer.fadeOut(500);
		
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 2000);
	});
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audioPieces[0].play();
			girlMouthIdle.fadeOut(0);
			girlMouth.fadeIn(0);
		}, 1000);
		timeout[1] = setTimeout(function(){
			acidPlusMetal.fadeIn(500);
		}, 8000);
		timeout[2] = setTimeout(function(){
			sprite[0].play();
		}, 9000);
		timeout[3] = setTimeout(function(){
			acidPlusMetal.fadeOut(500);
			acidPlusMagnesium.fadeIn(500);
		}, 16000);
		timeout[4] = setTimeout(function(){
			sprite[1].play();
		}, 17000);
		timeout[5] = setTimeout(function(){
			acidPlusMagnesium.fadeOut(500);
			videoContainer.fadeIn(0);
			video[0].play();
		}, 23000);
	};
}

launch["frame-108"] = function()
{		
		theFrame = $("#frame-108"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		girlMouth = $(prefix + ".girl-mouth"),
		girlMouthIdle = $(prefix + ".girl-mouth-idle"),
		boyMouth = $(prefix + ".boy-mouth"),
		demoPics = $(prefix + ".demo-pic"), 
		activeMetalsPlusAcid = $(prefix + ".active-metals-plus-acid"), 
		inactiveMetalsPlusAcid = $(prefix + ".inactive-metals-plus-acid");
	
	sprite[0] = new Motio(activeMetalsPlusAcid[0], {
		"fps": "1", 
		"frames": 9
	});
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	sprite[1] = new Motio(inactiveMetalsPlusAcid[0], {
		"fps": "1", 
		"frames": 8
	});
	sprite[1].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	demoPics.fadeOut(0);
	girlMouth.fadeOut(0);
	boyMouth.fadeOut(0);
	fadeNavsIn();
	
	audioPieces[0] = new AudioPiece("s4-1", 29, 59);
	
	audioPieces[0].addEventListener("ended", function(){
		girlMouth.fadeOut(0);
		girlMouthIdle.fadeIn(0);
		inactiveMetalsPlusAcid.fadeOut(500);
		
		timeout[0] = setTimeout(function(){
			fadeNavsIn();
		}, 2000);
	});
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			audioPieces[0].play();
			boyMouth.fadeIn(0);
		}, 1000);
		timeout[1] = setTimeout(function(){
			boyMouth.fadeOut(0);
		}, 5000);
		timeout[2] = setTimeout(function(){
			girlMouthIdle.fadeOut(0);
			girlMouth.fadeIn(0);
		}, 5000);
		timeout[3] = setTimeout(function(){
			activeMetalsPlusAcid.fadeIn(500);
		}, 11000);
		timeout[4] = setTimeout(function(){
			sprite[0].play();
		}, 12000);
		timeout[5] = setTimeout(function(){
			activeMetalsPlusAcid.fadeOut(500);
		}, 22000);
		timeout[6] = setTimeout(function(){
			inactiveMetalsPlusAcid.fadeIn(500);
		}, 22000);
		timeout[7] = setTimeout(function(){
			sprite[1].play();
		}, 23000);
	};
}

launch["frame-109"] = function()
{		
		theFrame = $("#frame-109"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyMouth = $(prefix + ".boy-mouth"),
		vocabulary = $(prefix + ".vocabulary");
	
	boyMouth.fadeOut(0);
	vocabulary.fadeOut(0);
	fadeNavsIn();
	
	audio[0] = new Audio("audio/s5-1.mp3");
	audioPieces[0] = new AudioPiece("s4-1", 63, 99);
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			boyMouth.fadeIn(0);
			audio[0].play();
			vocabulary.fadeIn(1000);
		}, 1000);
		timeout[1] = setTimeout(function(){
			audioPieces[0].play();
			boyMouth.fadeOut(0);
		}, 4000);
		timeout[2] = setTimeout(function(){
			
		}, 13000);
		timeout[3] = setTimeout(function(){
			fadeNavsIn();
		}, 16000);
	};
}

launch["frame-110"] = function()
{		
		theFrame = $("#frame-110"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		answers = $(prefix + ".answer"),
		keepThinking = $(prefix + ".keep-thinking"),
		instruction = $(prefix + ".instruction"),
		tasks = $(".task"),
		activeTask = 1;
	
	audio[0] = new Audio("audio/applause.mp3");
	
	tasks.fadeOut(0);
	keepThinking.fadeOut(0);
	instruction.fadeOut(0);
		
	fadeNavsIn();
	
	var answersListener = function(){
		var empty = $(prefix + ".task-" + activeTask + " .empty"),
			tempImage = $(this).css("background-image");
		empty.css("background-image", tempImage);
		
		if($(this).attr("data-correct"))
		{
			empty.css("background-color", "#01A6E8");
			audio[0].play();
			timeout[0] = setTimeout(function(){
				activeTask++;
				tasks.fadeOut(0); 
				$(".task-"+activeTask).fadeIn(0);
				empty = $("#frame-102 .task-" + activeTask + " .empty");
				if(activeTask > 3)
				{
					tasks.fadeOut(500);
					instruction.fadeOut(500);
					fadeNavsIn();
				}
			}, 2000);
		}
		else
		{
			empty.css("background-color", "#FF3C25");
			keepThinking.fadeIn(0);
			timeout[0] = setTimeout(function(){
				keepThinking.fadeOut(0);
				empty.css("background-image", "");
				empty.css("background-color", "");
			}, 2000);
		}
	};
	answers.off("click", answersListener);
	answers.on("click", answersListener);
	
	startButtonListener = function(){
		$(".task-"+activeTask).fadeIn(500);
		instruction.fadeIn(500);
	};
}

launch["frame-111"] = function()
{		
		theFrame = $("#frame-111"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		boyMouth = $(prefix + ".boy-mouth"),
		girlMouth = $(prefix + ".girl-mouth"),
		girlMouthIdle = $(prefix + ".girl-mouth-idle"), 
		carbonates = $(prefix + ".carbonates");
		
	sprite[0] = new Motio(carbonates[0], {
		"fps": "1.5",
		"frames": "11"
	});
	sprite[0].on("frame", function(){
		if(this.frame === this.frames - 1)
			this.pause();
	});
	
	boyMouth.fadeOut(0);
	girlMouth.fadeOut(0);
	fadeNavsIn();
	carbonates.fadeOut(0);
	
	audio[0] = new Audio("audio/s7-1.mp3");
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			boyMouth.fadeIn(0);
			audio[0].play();
		}, 1000);
		timeout[1] = setTimeout(function(){
			boyMouth.fadeOut(0);
		}, 4000);
		timeout[2] = setTimeout(function(){
			girlMouth.fadeIn(0);
			girlMouthIdle.fadeOut(0);
		}, 5000);
		timeout[3] = setTimeout(function(){
			carbonates.fadeIn(500);
		}, 6000);
		timeout[4] = setTimeout(function(){
			sprite[0].play();
		}, 7000);
		timeout[4] = setTimeout(function(){
			fadeNavsIn();
		}, 14000);
	};
}

launch["frame-112"] = function()
{		
		theFrame = $("#frame-112"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		girlMouth = $(prefix + ".girl-mouth"),
		girlMouthIdle = $(prefix + ".girl-mouth-idle"), 
		videoContainer = $(prefix + ".video-container"),
		video = $(prefix + ".video");
	
	video.attr("width", videoContainer.css("width"));
	video.attr("height", videoContainer.css("height"));
	
	$(window).resize(function(){
		video.attr("width", videoContainer.css("width"));
		video.attr("height", videoContainer.css("height"));
	});
	
	girlMouth.fadeOut(0);
	fadeNavsIn();
	videoContainer.fadeOut(0);
	
	audio[0] = new Audio("audio/s9-1.mp3");
	
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			girlMouthIdle.fadeOut(0);
			girlMouth.fadeIn(0);
			audio[0].play();
		}, 1000);
		timeout[1] = setTimeout(function(){
			videoContainer.fadeIn(500);
			video[0].play();
		}, 5000);
		timeout[2] = setTimeout(function(){
			videoContainer.fadeOut(500);
			girlMouthIdle.fadeIn(0);
			girlMouth.fadeOut(0);
		}, 28000);
		timeout[3] = setTimeout(function(){
			fadeNavsIn();
		}, 29000);
	};
}

launch["frame-113"] = function()
{		
		theFrame = $("#frame-113"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		draggables = $(prefix + ".ball");
	var draggabillies = [],
		vegetable, basket
		
	fadeNavsIn();
	
	audio[0] = new Audio("audio/s10-1.mp3");
	
	for (var i = 0; i < draggables.length; i ++)
		draggabillies[i] = new Draggabilly(draggables[i]);
	
	var onStart = function(instance, event, pointer)
	{
		vegetable = $(event.target);
	};
	var onEnd= function(instance, event, pointer)
	{
		vegetable.fadeOut(0);
		basket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		
		if(vegetable.attr("data-key") === basket.attr("data-key"))
		{
			basket.html(vegetable.html());
			basket.css("background-color", "#4EC7DD");
			vegetable.remove();
			if(!$(prefix + ".ball").length)
			{
				fadeNavsIn();
			}
		}	
		else
		{
			vegetable.fadeIn(0);
			vegetable.css("left", "");
			vegetable.css("top", "");
		}	
	};
	
	for (var i = 0; i < draggabillies.length; i ++)
	{
		draggabillies[i].on("dragStart", onStart);
		draggabillies[i].on("dragEnd", onEnd);
	}
	
	startButtonListener = function(){
		audio[0].play();
	};
}

launch["frame-114"] = function()
{		
		theFrame = $("#frame-114"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		mouths = $(prefix + ".mouths");
	
	mouths.fadeOut(0);
	fadeNavsIn();
	
	audio[0] = new Audio("audio/s11-1.mp3");
		
	startButtonListener = function(){
		timeout[0] = setTimeout(function(){
			mouths.fadeIn(0);
			audio[0].play();
		}, 1000);
		timeout[1] = setTimeout(function(){
			mouths.fadeOut(0);
		}, 4000);
		timeout[2] = setTimeout(function(){
			fadeNavsIn();
		}, 6000);
	};
}

launch["frame-301"] = function()
{
		theFrame = $(".task.frame"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		nextButton = $(prefix + ".next-button"),
		parts = $(prefix + ".part"),
		answers = $(prefix + ".answer"), 
		result = $(prefix + ".label"), 
		correctNum = 0;
	
	resultLabel = result.html();
	
	parts.fadeOut(0);
	$("part-1").fadeIn(0);
	
	answers.on("click", function(){
		if($(this).attr("data-correct"))
		{
			correctNum ++;
			result.html(resultLabel + " " + correctNum);
		}
	});
}

var hideEverythingBut = function(elem)
{
	var frames = $(".frame");
	frames.fadeOut(0);
	elem.fadeIn(0);
	
	console.log(audio.length);
	
	for(var i = 0; i < audio.length; i ++)
		audio[i].pause();
	
	for(var i = 0; i < audioPieces.length; i ++)
		audioPieces[i].pause();
	
	for(var i = 0; i < timeout.length; i++)
		clearTimeout(timeout[0]);
	
	launch[elem.attr("id")]();
}

var initMenuButtons = function(){
	$(".link").on("click", function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var showModel = function(content){
	model.fadeIn(1000);
	modelContent.html(content);
};

var hideModel = function(){
	model.fadeOut(1000);
};

var showMap = function()
{
	
}
var main = function()
{
	initMenuButtons();
	hideEverythingBut($("#frame-000"));
	//launch107();
};

$(document).ready(main);